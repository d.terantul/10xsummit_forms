<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Оплата билета на САММИТ 10Х</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css?ver='1.03'">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="heder">
                <div class="main-heder">
                    <div class="logo"><img src="img\logotype.png" alt="Logotipe"></div>
                    <div class="contacts">
                        <div class="left">
                            <p>Остались вопросы? Звоните!</p>
                        </div>
                        <div class="right">
                            <div class="ru">
                                <p class="text-c">Россия:</p>
                                <p>+7 495 133 82 24</p>
                            </div>
                            <div class="uk">
                                <p class="text-c">Украина:</p>
                                <p>+38 096 916 13 88</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="caption">
                    <h1><b>Оплата билета на САММИТ 10Х</b></h1>
                    <p style="font-size: 26px;">Оплачивайте сейчас и получите скидку <span style="color: #ff6700; font-weight: 600">5%</span></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="content">
                <div class="block-left">
                    <h2><b>На САММИТЕ 10Х получите<br>  протестированные технологии новой эры:</b></h2>
                    <ul>
                        <li><p>10Х ПРОДАЖИ.</p><p class="text-c">Инновационная (и уже много раз протестированная в разных сферах бизнеса) система продаж, которая помогает «закрывать» в 5-7 раз больше клиентов, чем ваши конкуренты сейчас. И вы наконец-то сможете инвестировать в рекламу в 2-5 раз больше, возвращая инвестиции почти мгновенно, масштабировать бизнес и развивая бренд.</p></li>
                        <li><p>10Х БРЕНД.</p><p class="text-c">Технология как методично перейти от небольшой группы «случайных» клиентов и подписчиков в соцсетях к масштабному сообществу единомышленников, которые доверяют вам, рассказывают о вас, рекомендуют вас и покупают у вас снова и снова с нулевым сопротивлением.</p></li>
                        <li><p>10Х ПОТОК.</p><p class="text-c">Самые перспективные каналы траффика и современные инструменты, которые дадут вам прогнозируемый 10-кратынй рост количества потенциальных клиентов в следующие 3-5 лет.</p></li>
                        <li><p>10Х КОМАНДА.</p><p class="text-c">Система как создать мотивированную команду из игроков А-класса, которая реализует любой, даже самый амбициозный план по щелчку пальца, не задавая лишних вопросов</p></li>
                    </ul>
                </div>
                <div class="block-right">
                    <div class="ticket">
                        <div class="for">
                            <p class="text-c">Билет на</p>
                            <p><b>"Саммит 10Х"</b></p>
                        </div>
                        <div class="date">
                            <p class="text-c">Дата: </p>
                            <p class="date">10-11 ноября</p>
                        </div>
                        <div class="member">
                            <p class="text-c">Участник:</p>
                            <h2><?=isset($_GET['firstname'])?$_GET['firstname']:''?></h2>
                        </div>
                        <div class="code">
                            <img src="img/code.png" alt="">
                        </div>
                    </div>
                    <div class="price">
                        <h2>Билет на грандиозный САММИТ 10Х</h2>
                        <p>Обычная цена - </p> <p class="text-c" style="text-decoration:line-through"><b>$97</b></p>
                        <p>Цена со скидкой - </p> <p class="text-c"><b>$92.15</b></p>
                    </div>
                    <div class="form">
                        <form action="oplata.php" method="POST">
                            <input type="email" class="input" placeholder="E-MAIL" name="email" value="<?=isset($_GET['email'])?$_GET['email']:''?>"><br>
                            <input type="submit" value="Оплатить картой">
                            <div class="link">
                                    <a href="https://goldcoach.ru/oplata/oplata.pdf">Другой способ оплаты</a><img src="img/icon-pdf.png" alt="PDF">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="foother">
                <p>02068, Украина, г. Киев, ул. Старонаводницкая, 6Б, оф.210<br>
                    Официальный адрес компании GoldCoach Ltd: Suite 1,4 Queen Street, Edinburgh, Scotland, EH21JE, United Kingdom</p>
                <ul>
                    <li><a href="http://goldcoach.ru/otkaz/" target="blank">Отказ от ответственности</a></li>
                    <li><a href="http://goldcoach.ru/agreement/" target="blank">Пользовательское соглашение</a></li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>