module.exports = {
    all: {
        options: {
	      separator: ';',
	    },
	    dist: {
	      src: 'dev/js/*.js',
	      dest: 'src/js/main.js',
	    }
    }
};
